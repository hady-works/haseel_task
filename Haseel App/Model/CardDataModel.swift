//
//  CardDataModel.swift
//  Haseel App
//
//  Created by Hady on 2/5/21.
//  Copyright © 2021 Haseel. All rights reserved.
//

import UIKit

struct CardDataModel {
    let id: Int
    let icon: UIImage?
    let cardNumber: String
}
