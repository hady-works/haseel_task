//
//  PaymentDataModel.swift
//  Haseel App
//
//  Created by Hady on 2/5/21.
//  Copyright © 2021 Haseel. All rights reserved.
//

import UIKit

struct PaymentDataModel {
    let unSelectedIcon: UIImage?
    let selectedIcon: UIImage?
}

