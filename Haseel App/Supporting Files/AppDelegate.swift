//
//  AppDelegate.swift
//  Haseel App
//
//  Created by Hady on 2/4/21.
//  Copyright © 2021 Haseel. All rights reserved.
//

import MOLH

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        MOLH.shared.activate(true)
        if MOLHLanguage.isRTLLanguage() {
            UICollectionView.appearance().semanticContentAttribute = .forceRightToLeft
        }
        return true
    }
    
}

extension AppDelegate: MOLHResetable {
    
    func reset() {
        let rootViewController: UIWindow = ((UIApplication.shared.delegate?.window)!)!
        rootViewController.rootViewController = CheckOutVC.instance()
    }
}
