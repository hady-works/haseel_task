//
//  UIView+Extensions.swift
//  Haseel App
//
//  Created by Hady on 2/4/21.
//  Copyright © 2021 Haseel. All rights reserved.
//

import UIKit

extension UIView{
    
    func addShadowWith(color: UIColor, radius: CGFloat, opacity: Float?) {
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.layer.shadowRadius = radius
        self.layer.shadowOpacity = opacity ?? 0.7
    }
    
    func addNormalShadow(){
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.layer.shadowRadius = 5
        self.layer.shadowOpacity = 0.2
    }
    
    func addCornerRadius(_ radius: CGFloat = 0) {
        if radius == 0 {
            self.layer.cornerRadius = self.frame.size.height / 2
        } else {
            self.layer.cornerRadius = radius
        }
    }
    
    func addBorderWith(width: CGFloat, color: UIColor) {
        self.layer.borderWidth = width
        self.layer.borderColor = color.cgColor
    }
    
    func roundedFromSide(corners: UIRectCorner, cornerRadius: Double) {
        
        let size = CGSize(width: cornerRadius, height: cornerRadius)
        let bezierPath = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: size)
        let shapeLayer = CAShapeLayer()
        shapeLayer.frame = self.bounds
        shapeLayer.path = bezierPath.cgPath
        self.layer.mask = shapeLayer
    }
    
    func rounded(borderWidth: CGFloat, cornerRadius: CGFloat, borderColor: UIColor) {
        self.layer.borderWidth = borderWidth
        self.layer.cornerRadius = cornerRadius
        self.layer.masksToBounds = true
        self.layer.borderColor = borderColor.cgColor
    }
    
    func shadow(shadowOpacity: Float? = 0.1, color: CGColor? = UIColor.black.cgColor) {
        layer.masksToBounds = false
        layer.shadowColor = color
        layer.shadowRadius = 8.0
        layer.shadowOffset = CGSize(width: -1, height: 1)
        layer.shadowOpacity = shadowOpacity ?? 0.1
    }
    
    func shadowWithCorner(shadowOpacity: Float? = 0.1, color: CGColor? = UIColor.black.cgColor) {
        self.layer.masksToBounds = false
        self.layer.shadowColor = color
        self.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.layer.shadowOpacity = shadowOpacity ?? 0.1
        self.layer.cornerRadius = 8
    }
    
    func dropShdow() {
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 1.0)
        layer.shadowOpacity = 0.1
        layer.shadowRadius = 6.0
        layer.masksToBounds = false
    }
    
    func setBorder( color: CGColor? = UIColor.lightGray.cgColor.copy(alpha: 0.5), width: CGFloat? = 0.5) {
        self.layer.borderColor = color ?? UIColor.lightGray.cgColor.copy(alpha: 0.5)
        self.layer.borderWidth = width ?? 0.5
    }
    
    func cornerRadiusView( radius: CGFloat? = nil) {
        self.layer.cornerRadius = radius ?? self.frame.width / 2
        self.layer.masksToBounds = true
    }
    
    func cornerRadiusForHeight() {
        self.layer.cornerRadius = self.frame.height / 2
        self.layer.masksToBounds = true
    }
    
    func cornerRadiusForHeight_(cornerRadius: Float) {
        self.layer.cornerRadius = CGFloat(cornerRadius)
        self.layer.masksToBounds = true
    }
    
    func applyGradient(colours: [UIColor], locations: [NSNumber]?) -> Void {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        gradient.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradient.endPoint = CGPoint(x: 0.8, y: 1.0)
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    func applyGradient_(colours: [UIColor], locations: [NSNumber]?, view: UIView?) -> Void {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = view?.bounds ?? self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        gradient.endPoint = CGPoint(x: 0.0, y: 0.0)
        gradient.startPoint = CGPoint(x: 0.8, y: 1.0)
        view!.layer.insertSublayer(gradient, at: 0)
    }
    
    func setGradient() {
        let firstColor = UIColor(displayP3Red: 95/255, green: 205/255, blue: 253/255, alpha: 1)
        let secondColor = UIColor(displayP3Red: 51/255, green: 147/255, blue: 189/255, alpha: 1)
        self.applyGradient(colours: [ firstColor, secondColor], locations: [0.0, 1.0])
    }
    
    func setGradient_(view: UIView?) {
        let firstColor = UIColor(displayP3Red: 95/255, green: 205/255, blue: 253/255, alpha: 1)
        let secondColor = UIColor(displayP3Red: 51/255, green: 147/255, blue: 189/255, alpha: 1)
        self.applyGradient_(colours: [firstColor, secondColor], locations: [0.0, 1.0], view: view)
    }
    
}
