//
//  UIViewController+Extensions.swift
//  Haseel App
//
//  Created by Hady on 2/4/21.
//  Copyright © 2021 Haseel. All rights reserved.
//

import MOLH
import FittedSheets

extension UIViewController {
    
    func NavigationBarClear() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.clear
    }
    
    @objc func onDismissKeyboard() {
        view.endEditing(true)
    }
    
    @objc func onDismiss() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func setCollectionAppearance() {
        UICollectionView.appearance().semanticContentAttribute = MOLHLanguage.isRTLLanguage() ? .forceRightToLeft : .forceLeftToRight
    }
    
    func presentSheetController(viewToPresent: BottomSheetVC, topCornersRadius: CGFloat? = 12, fullScreenModel: Bool = false, height: CGFloat, bottomControl: Bool = true){
        
        let mySize: SheetSize = .fixed(CGFloat(height))
        var sheet = SheetViewController(controller: viewToPresent, sizes: [mySize])
        if fullScreenModel{
            sheet = SheetViewController(controller: viewToPresent, sizes: [mySize, .fullscreen])
        }
        
        sheet.allowPullingPastMaxHeight = false
        sheet.cornerRadius = topCornersRadius ?? 6
        //sheet.handleColor = UIColor.clear
        viewToPresent.sheetCtl = sheet
        self.present(sheet, animated: false, completion: nil)
    }
    
}
