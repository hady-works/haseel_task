//
//  UIColor+Extensions.swift
//  Haseel App
//
//  Created by Hady on 2/5/21.
//  Copyright © 2021 Haseel. All rights reserved.
//

import UIKit

extension UIColor {
    
    @nonobjc class var borderColor: UIColor {
        return UIColor(red: 72 / 255.0, green: 189 / 255.0, blue: 118 / 255.0, alpha: 1)
    }
    
    @nonobjc class var visaBackground: UIColor {
        return UIColor(red: 244 / 255.0, green: 244 / 255.0, blue: 244 / 255.0, alpha: 1)
    }
    
    @nonobjc class var unSelectedBackground: UIColor {
        return UIColor(red: 241 / 255.0, green: 242 / 255.0, blue: 243 / 255.0, alpha: 1)
    }
}
