//
//  PaymentMethodCVCell.swift
//  Haseel App
//
//  Created by Hady on 2/5/21.
//  Copyright © 2021 Haseel. All rights reserved.
//

import UIKit

class PaymentMethodCVCell: UICollectionViewCell {
    
    //MARK:- Outlet's
    @IBOutlet weak var paymentIcon: UIImageView!
    
    //MARK:- Properties
    override var isSelected: Bool {
        didSet{
            paymentIcon.image = isSelected ? image?.selectedIcon : image?.unSelectedIcon
        }
    }
    
    var image: PaymentDataModel?{
        didSet{
            paymentIcon.image = image?.unSelectedIcon
        }
    }
    
    //MARK:- LifeCycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
