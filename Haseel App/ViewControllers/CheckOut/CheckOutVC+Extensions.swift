//
//  CheckOutVC+Extensions.swift
//  Haseel App
//
//  Created by Hady on 2/4/21.
//  Copyright © 2021 Haseel. All rights reserved.
//

import UIKit

extension CheckOutVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = productTV.dequeue() as ProductTVCell
        return cell
    }
}

extension CheckOutVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return paymentMethods.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = paymentMethodsCV.dequeue(indexPath: indexPath) as PaymentMethodCVCell
        cell.image = paymentMethods[indexPath.row]
        cell.isSelected = (indexPath.row == paymentIndex)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        paymentIndex = indexPath.row
        let vc = SelectVisaVC.instance()
        vc.delegate = self
        self.presentSheetController(viewToPresent: vc, topCornersRadius: 12, fullScreenModel: false, height: 500)
        paymentMethodsCV.reloadData()
    }
    
}

extension CheckOutVC: SelectCardDelegate{
    func selectCard(card: CardDataModel) {
        completeBtn.setTitleColor(.white, for: .normal)
        completeBtn.backgroundColor = .borderColor
        selectedCard = card
        seperatorView.isHidden = false
        cardView.isHidden = false
        cardIcon.image = card.icon
        cardNumberLbl.text = card.cardNumber
    }
}
