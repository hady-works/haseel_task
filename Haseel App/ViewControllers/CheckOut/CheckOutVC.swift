//
//  CheckOutVC.swift
//  Haseel App
//
//  Created by Hady on 2/4/21.
//  Copyright © 2021 Haseel. All rights reserved.
//

import MOLH

class CheckOutVC: UIViewController {
    
    //MARK:- Outlet's
    @IBOutlet weak var paymentMethodsCV: UICollectionView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var productTV: UITableView!
    @IBOutlet weak var moreProductsBtn: UIButton!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var completeBtn: UIButton!
    @IBOutlet weak var hideIcon: UIImageView!
    @IBOutlet weak var seperatorView: UIView!
    @IBOutlet weak var cardIconView: UIView!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var cardIcon: UIImageView!
    @IBOutlet weak var cardNumberLbl: UILabel!
    @IBOutlet weak var languageBtn: UIButton!
    
    //MARK:- Instance
    static func instance()-> CheckOutVC{
        let story = UIStoryboard(name: "Main", bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: "CheckOutVC") as! CheckOutVC
        return vc
    }
    
    //MARK:- Variables
    var paymentMethods = [PaymentDataModel]()
    var paymentIndex = 8
    var count = 8
    var isShown = false
    var selectedCard: CardDataModel?
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        loadPaymentMethods()
        setupUi()
        setupCVAndTV()
    }
    
    //MARK:- Helper's
    func setupCVAndTV(){
        paymentMethodsCV.delegate = self
        paymentMethodsCV.dataSource = self
        paymentMethodsCV.registerCellNib(cellClass: PaymentMethodCVCell.self)
        productTV.delegate = self
        productTV.dataSource = self
        productTV.registerCellNib(cellClass: ProductTVCell.self)
    }
    
    func setupUi(){
        languageBtn.isSelected = MOLHLanguage.isArabic()
        hideIcon.isHidden = true
        count = count > 3 ? 3 : count
        topView.layer.layerShadow(color: .black, alpha: 0.03, x: 0, y: 6, blur: 12, spread: 0)
        moreProductsBtn.underline()
        bottomView.layer.layerShadow(color: .black, alpha: 0.03, x: 0, y: -3, blur: 12, spread: 0)
        completeBtn.addCornerRadius(8)
        cardIconView.addCornerRadius(8)
        seperatorView.isHidden = true
        cardView.isHidden = true
    }
    
    func loadPaymentMethods(){
        paymentMethods.append(PaymentDataModel(unSelectedIcon: UIImage(named: "Apple Pay"), selectedIcon: UIImage(named: "Apple Pay-select")))
        paymentMethods.append(PaymentDataModel(unSelectedIcon: UIImage(named: "Cash Copy"), selectedIcon: UIImage(named: "Cash Copy-select")))
        paymentMethods.append(PaymentDataModel(unSelectedIcon: UIImage(named: "Cash"), selectedIcon: UIImage(named: "Cash-select")))
        paymentMethods.append(PaymentDataModel(unSelectedIcon: UIImage(named: "Mada-Visa"), selectedIcon: UIImage(named: "Mada-Visa-select")))
        paymentMethods.append(PaymentDataModel(unSelectedIcon: UIImage(named: "stcPay"), selectedIcon: UIImage(named: "stcPay-select")))
    }
    
    func changeBtnAttributedTitle(text: String, btn: UIButton){
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 0, length: text.count))
        btn.setAttributedTitle(attributedString, for: .normal)
    }
    
    //MARK:- Action's
    @IBAction func backBtnAction(_ sender: Any) {
        print("Back")
    }
    
    @IBAction func moreBtnAction(_ sender: Any) {
        hideIcon.isHidden = isShown
        isShown = !isShown
        let btnTitle = isShown ? "Hide".localized : "Products 5+".localized
        changeBtnAttributedTitle(text: btnTitle, btn: moreProductsBtn)
        count = isShown ? 8 : 3
        productTV.reloadData()
    }
    
    @IBAction func completeBtnAction(_ sender: Any) {
        print("Complete")
    }
    
    @IBAction func changeCardBtnAction(_ sender: Any) {
        let vc = SelectVisaVC.instance()
        vc.delegate = self
        vc.selectedCard = self.selectedCard
        self.presentSheetController(viewToPresent: vc, topCornersRadius: 12, fullScreenModel: false, height: 500)
    }
    
    @IBAction func changeAppLanguageBtnAction(_ sender: Any) {
        if MOLHLanguage.currentAppleLanguage() == "ar"{
            MOLHLanguage.setDefaultLanguage("en")
            MOLH.setLanguageTo("en")
            languageBtn.isSelected = false
            setCollectionAppearance()
            MOLH.reset()
        }else if MOLHLanguage.currentAppleLanguage() == "en"{
            MOLHLanguage.setDefaultLanguage("ar")
            MOLH.setLanguageTo("ar")
            languageBtn.isSelected = true
            setCollectionAppearance()
            MOLH.reset()
        }
    }
    
}
