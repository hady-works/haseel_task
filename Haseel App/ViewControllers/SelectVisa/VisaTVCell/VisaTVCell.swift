//
//  VisaTVCell.swift
//  Haseel App
//
//  Created by Hady on 2/5/21.
//  Copyright © 2021 Haseel. All rights reserved.
//

import UIKit

class VisaTVCell: UITableViewCell {
    
    //MARK:- Outlet's
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var visaIconView: UIView!
    @IBOutlet weak var visaIcon: UIImageView!
    @IBOutlet weak var cardNumberLbl: UILabel!
    @IBOutlet weak var selectedIconBtn: UIButton!
    
    //MARK:- Properties
    var card: CardDataModel?{
        didSet{
            visaIcon.image = card?.icon
            cardNumberLbl.text = card?.cardNumber
        }
    }
    
    //MARK:- LifeCycle
    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.addCornerRadius(8)
        visaIconView.addCornerRadius(8)
    }
    
    override var isSelected: Bool {
        didSet{
            containerView.backgroundColor = isSelected ? .white : .unSelectedBackground
            containerView.addBorderWith(width: 2, color: isSelected ? .borderColor : .unSelectedBackground)
            visaIconView.backgroundColor = isSelected ? .visaBackground : .white
            selectedIconBtn.isSelected = isSelected
            cardNumberLbl.textColor = isSelected ? .borderColor : .black
        }
    }
    
}
