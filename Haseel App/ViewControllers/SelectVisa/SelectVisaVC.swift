//
//  SelectVisaVC.swift
//  Haseel App
//
//  Created by Hady on 2/5/21.
//  Copyright © 2021 Haseel. All rights reserved.
//

import UIKit

protocol SelectCardDelegate {
    func selectCard(card: CardDataModel)
}

class SelectVisaVC: BottomSheetVC {
    
    //MARK:- Outlet's
    @IBOutlet weak var visaTV: UITableView!
    @IBOutlet weak var addCardView: UIView!
    
    //MARK:- Instance
    var delegate: SelectCardDelegate?
    var selectedCard: CardDataModel?
    var cardsArray = [CardDataModel]()
    static func instance()-> SelectVisaVC{
        let story = UIStoryboard(name: "Main", bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: "SelectVisaVC") as! SelectVisaVC
        vc.sheetHeight = 500
        return vc
    }
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        loadCards()
        setupTV()
        setupUi()
    }
    
    //MARK:- Helper's
    func setupTV(){
        visaTV.delegate = self
        visaTV.dataSource = self
        visaTV.registerCellNib(cellClass: VisaTVCell.self)
    }
    
    func setupUi(){
        addCardView.addCornerRadius(8)
        addCardView.addBorderWith(width: 1, color: .borderColor)
    }
    
    func loadCards(){
        cardsArray.append(CardDataModel(id: 1, icon: UIImage(named: "mada_icon"), cardNumber: "7992  ●●●●  ●●●●  ●●●●  ●●●●"))
        cardsArray.append(CardDataModel(id: 2, icon: UIImage(named: "visa_icon"), cardNumber: "8800  ●●●●  ●●●●  ●●●●  ●●●●"))
    }
    
}
