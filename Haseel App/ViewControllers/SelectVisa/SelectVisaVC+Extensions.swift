//
//  SelectVisaVC+Extensions.swift
//  Haseel App
//
//  Created by Hady on 2/5/21.
//  Copyright © 2021 Haseel. All rights reserved.
//

import UIKit

extension SelectVisaVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cardsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = visaTV.dequeue() as VisaTVCell
        let card = cardsArray[indexPath.row]
        cell.card = card
        cell.isSelected = (card.id == selectedCard?.id)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedCard = cardsArray[indexPath.row]
        visaTV.reloadData()
        delegate?.selectCard(card: cardsArray[indexPath.row])
    }
}
