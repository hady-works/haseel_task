//
//  LocalizedLabel.swift
//  Haseel App
//
//  Created by Hady on 2/4/21.
//  Copyright © 2021 Haseel. All rights reserved.
//

import UIKit
import MOLH

class LocalizedLabel: UILabel{
    
    override func awakeFromNib() {
        if let txt = self.text{
            self.text = txt.localized
        }
        adjustAutoAlignment()
    }
    
    func adjustAutoAlignment(){
        if self.textAlignment != .center {
            if MOLHLanguage.isArabic() {
                self.textAlignment = .right
            }else{
                self.textAlignment = .left
            }
        }
    }
}
