//
//  LocalizedTextField.swift
//  Haseel App
//
//  Created by Hady on 2/4/21.
//  Copyright © 2021 Haseel. All rights reserved.
//

import UIKit
import MOLH
@IBDesignable

class LocalizedTextField: UITextField {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if MOLHLanguage.isArabic() {
            if let placeHolder = self.placeholder {
                self.placeholder = placeHolder.localized
            }
        }
        adjustAutoAlignment()
    }
     
     func adjustAutoAlignment(){
         if self.textAlignment != .center {
             if MOLHLanguage.isArabic() {
                 self.textAlignment = .right
             }else{
                 
                 self.textAlignment = .left
             }
         }
     }
}
