//
//  LocalizedButton.swift
//  Haseel App
//
//  Created by Hady on 2/4/21.
//  Copyright © 2021 Haseel. All rights reserved.
//

import UIKit
import MOLH

class LocalizedButton: UIButton{
    
    override func awakeFromNib() {
        self.setTitle(self.currentTitle?.localized, for: .normal)
    }
    
}
