//
//  LocalizedCollectionView.swift
//  Haseel App
//
//  Created by Hady on 2/4/21.
//  Copyright © 2021 Haseel. All rights reserved.
//

import UIKit
import MOLH

class LocalizedCollectionView: UICollectionViewFlowLayout {
    override var flipsHorizontallyInOppositeLayoutDirection: Bool {
        return MOLHLanguage.isArabic()
    }
}
