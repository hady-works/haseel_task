//
//  LocalizedButtonFlippedDirection.swift
//  Haseel App
//
//  Created by Hady on 2/4/21.
//  Copyright © 2021 Haseel. All rights reserved.
//

import UIKit
import MOLH

class LocalizedButtonFlippedDirection: UIButton{
    
    override func awakeFromNib() {
        flippedDirection()
    }
    func flippedDirection(){
        if MOLHLanguage.isArabic() {
            self.imageView?.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi));
        }
    }
}
